module.exports = {
	use    : [
		'postcss-import',
		'autoprefixer',
		'postcss-css-variables',
		'postcss-custom-selectors',
		'stylelint'
	],
	input  : './css/main.css',
	output : './main.css',
	'postcss-import' : {
  		onImport: function(sources) {
  			   global.watchCSS(sources);
  		}
	}
};
